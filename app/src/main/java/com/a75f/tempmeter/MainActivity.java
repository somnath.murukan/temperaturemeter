package com.a75f.tempmeter;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.marcinmoskala.arcseekbar.ArcSeekBar;
import com.marcinmoskala.arcseekbar.ProgressListener;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private ArcSeekBar arcSeekBar;
    private TextView tv_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arcSeekBar = findViewById(R.id.seekArc);
        tv_progress = findViewById(R.id.tv_progress);

        arcSeekBar.setMaxProgress(100);
        arcSeekBar.setOnProgressChangedListener(progressListener);
    }

    ProgressListener progressListener = new ProgressListener() {
        @Override
        public void invoke(int i) {
            if (i==100) {
                arcSeekBar.setProgressGradient(getResources().getColor(R.color.red), getResources().getColor(R.color.red));
                arcSeekBar.setProgressBackgroundGradient(getResources().getColor(R.color.OrangeRed), getResources().getColor(R.color.OrangeRed));
            }
            else if (i == 0) {
                arcSeekBar.setProgressGradient(getResources().getColor(R.color.RoyalBlue), getResources().getColor(R.color.RoyalBlue));
                arcSeekBar.setProgressBackgroundGradient(getResources().getColor(R.color.RoyalBlue), getResources().getColor(R.color.RoyalBlue));
            }
            else {
                arcSeekBar.setProgressGradient(getResources().getColor(R.color.RoyalBlue), Color.RED);
                arcSeekBar.setProgressBackgroundGradient(getResources().getColor(R.color.RoyalBlue), Color.RED);
            }
            double progress = 50+(0.5*i);
            DecimalFormat format = new DecimalFormat("###.#");
            tv_progress.setText(format.format(progress));
        }
    };
}
